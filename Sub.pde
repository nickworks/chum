class Sub extends Enemy {

  float pilotHeight;

  Sub(float x) {
    super(x);
    y = 500;
    setImg(statePlay.imgSub);
    pilotHeight = random(100, 1000);
    radius = 20;
    hp = 30;
  }
  void update() {
    if (dead) {
      rotation += speedR;
      speedR *= .99;
    } else {

      if (statePlay.hero.x > x + targetXOffset) speedX += .1;
      if (statePlay.hero.x < x + targetXOffset) speedX -= .1;

      float targetY = statePlay.hero.y; 
      float limitMinDepth = statePlay.water.base + statePlay.water.amp1 + statePlay.water.amp2 + pilotHeight;
      if (targetY < limitMinDepth) targetY = limitMinDepth;

      if (targetY > y + targetYOffset) speedY += .1;
      if (targetY < y + targetYOffset) speedY -= .1;
    }
    super.update();
  }
  void kill() {
    img = statePlay.imgSubX;
    super.kill();
    statePlay.explosionAt(this);
    sinker = true;
    statePlay.addScore(200, true);
    speedR = random(-.05, .05);
  }
  void draw() {
    flipHorizontal = !dead && speedX < 0;
    super.draw();
  }
}

