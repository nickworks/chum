class EnvSprite {
  float x;
  float y;
  float speedX;
  float speedY;
  PImage img;
  float limitX = 1000;
  boolean inScreenSpace = false;

  void wrapPosition() {
    float disToHero = x - statePlay.hero.x;
    if (disToHero > limitX) x -= limitX * 2 - 50;
    else if (disToHero < -limitX) x += limitX * 2 - 50;
  }
  void update() {
    x += speedX;
    y += speedY;
    wrapPosition();
  }
  void draw() {
    float tx = x;
    if(inScreenSpace) tx = statePlay.cam.worldToScreenX(tx);
    
    image(img, tx, y);
  }
}
class Cloud extends EnvSprite {
  Cloud(PImage img) {
    this.img = img;
    y = random(-400, -1);
    x = random(-1000, 1000);
    speedX = random(-2, 2);
  }
  void draw() {
    tint(255, 128);
    super.draw();
  }
}
class Wreck extends EnvSprite {
  Wreck(float bottom) {
    inScreenSpace = true;
    img = loadImage("wreck.png");
    x = 0;
    y = bottom + 70;
  }
  void draw() {
    tint(255);
    if(img != null) super.draw();
  }
}
