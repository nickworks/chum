class Boat extends Enemy {

  float acceleration = random(.001, .01);
  float maxSpeed = random(.2, .5) + random(.2, .5);
  boolean speedBoat = false;

  Boat(float x, boolean speedBoat) {
    super(x);
    this.speedBoat = speedBoat;
    snapToSurface();
    setImg(statePlay.imgBoat);
    regY += 5;
    bouyant = true;
    hp = 30;

    if (speedBoat) {
      maxSpeed = 2;
      acceleration = .05;
    }
  }  
  void update() {
    if (!dead) {

      if (bouyant && !speedBoat) {
        snapToSurface();
        speedY = 0;
      }
      if (statePlay.hero.x > x + targetXOffset) speedX += acceleration;
      if (statePlay.hero.x < x + targetXOffset) speedX -= acceleration;

      if (speedX > maxSpeed) speedX = maxSpeed;
      if (speedX <-maxSpeed) speedX = -maxSpeed;
    }
    x += speedX;

    super.update();
    float rotationTarget = statePlay.water.getSlopeAt((int)x);

    if (rotation > rotationTarget) rotation -= .01;
    if (rotation < rotationTarget) rotation += .01;
  }
  void kill() {
    img = statePlay.imgBoatX;
    super.kill();
    statePlay.explosionAt(this);
    bouyant = false;
    sinker = true;
    statePlay.addScore(100, true);
  }
}

