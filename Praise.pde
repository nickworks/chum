class Praise {

  int mode; // slide in, stamp in, type in, etc
  int step = 0;
  int nextStepAt = 0;
  int alpha = 255;

  float x = 0;
  float y = 0;
  float rotation = 0;
  float size = 1;
  String[] choices = new String[] {
    "DIE HUMANS!", 
    "YUMMY\nGIBLETS!", 
    "FEEDING\nFRENZY", 
    "BOOM GOES\nTHE DYNAMITE", 
    "PREHISTORIC\nCARNAGE", 
    "EVOLUTIONARY\nPERFECTION", 
    "DELICIOUS,\nDERPY HUMANS!",
    "BIG-ASS\nMUTLIPLIER",  
    "MUTHA 'UCKIN\nRIDICULOUS", 
    "SLEEP WITH\nTHEM FISHES", 
    "CHUM-TASTIC!",
    "HATERS\nGONNA HATE"
  };
  String words;

  Praise() {
    int mult = statePlay.multiplier;
    words = choices[(int)random(choices.length)];
    mode = (int)random(0, 4);
    switch(mode) {
    case 0:
      y = height;
      break;
    case 1:
      x = -width;
      break;
    case 2:
      size = 0;
      break;
    case 3:
      rotation = PI;
      break;
    }
  }
  void update() {

    if (statePlay.time > nextStepAt) {
      nextStepAt = statePlay.time + 1000;
      step++;
    }

    if (step == 1) {
      if (x != 0) x += (0 - x) * .2;
      if (y != 0) y += (0 - y) * .2;
      if (rotation != 0) rotation += (0 - rotation) * .1;
      if (size != 1) size += (1 - size) * .1;
    } 
    else {
      alpha -= 5;
      if (alpha <= 0) alpha = 0;
    }
  }
  void draw() {
    if(alpha <= 0) return;
    pushMatrix();
    translate(400, 250);
    scale(size, size);
    rotate(rotation);
    fill(255, alpha);
    textFont(fontBig, 80);
    textAlign(CENTER);
    text(words, x, y);
    popMatrix();
  }
}

